/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import 'babel-polyfill';
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import React from 'react'; // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom/server';
import proxyMiddleware from 'http-proxy-middleware';
import PrettyError from 'pretty-error';
import Html from './components/Html';
import ErrorPage from './components/Error';
import errorPageStyle from './components/Error/ErrorPage.css';
import { port, proxyTable } from './config';
import assets from './assets'; // eslint-disable-line import/no-unresolved

const app = express();

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
// serve pure static assets
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
Object.keys(proxyTable).forEach((context) => {
    let options = proxyTable[context];
    if (typeof options === 'string') {
        options = { target: options };
    }
    app.use(proxyMiddleware(context, options));
});

//
// Register server-side routes middleware to
// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
// -----------------------------------------------------------------------------
app.get('/', (req, res, next) => {
    try {
        res.status(200).sendFile(path.resolve(__dirname, 'public', 'index.html'));
    } catch (err) {
        next(err);
    }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
    console.log(pe.render(err)); // eslint-disable-line no-console
    const html = ReactDOM.renderToStaticMarkup(
        <Html
            title="Internal Server Error"
            description={err.message}
            style={errorPageStyle._getCss()} // eslint-disable-line no-underscore-dangle
            script={assets.main.js}
        >
            {ReactDOM.renderToString(<ErrorPage error={err} />)}
        </Html>
    );
    res.status(err.status || 500);
    // res.send(err.message);
    res.send(`<!DOCTYPE html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
/* eslint-disable no-console */
app.listen(port, () => {
    console.log(`The server is running at http://localhost:${port}/`);
});
/* eslint-enable no-console */
