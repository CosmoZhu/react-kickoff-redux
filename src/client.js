import 'babel-polyfill';
import React from 'react'; // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import { Router, Route, hashHistory } from 'react-router';
import App from './components/APP';
import Demo from './components/Demo';

function bootstrap() {
    // Make taps on links and buttons work fast on mobiles
    if (FastClick.attach) {
        FastClick.attach(document.body);
    } else {
        FastClick(document.body);
    }

    ReactDOM.render(
        (<Router history={hashHistory}>
            <Route path="/" component={App}>
                <Route path="/about" component={Demo} />
            </Route>
          </Router>), document.getElementById('reactapp')
        );
}

// Run the application when both DOM is ready and page content is loaded
if (['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {
    bootstrap();
} else {
    document.addEventListener('DOMContentLoaded', bootstrap, false);
}
