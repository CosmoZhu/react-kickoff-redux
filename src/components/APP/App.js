import React, { PropTypes } from 'react';

function App(props) {
    return (
        <div>
            <section>
                {props.children || 'Welcome to React kick off'}
            </section>
        </div>
    );
}

App.propTypes = {
    children: PropTypes.element,
};

export default App;
