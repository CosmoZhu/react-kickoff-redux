import React from 'react';
import './Demo.less';

function Demo() {
    return (
        <div>
            <h2 className="cells">About</h2>
            <p>React kickoff aims to give you a good starting point for your projects.</p>
            <p>If you're looking for a ES6 (ES2015) React JS starter with nice shallow rendering test examples, this is probably for you.</p>
        </div>
    );
}

export default Demo;
